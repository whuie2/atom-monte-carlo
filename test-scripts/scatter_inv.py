import numpy as np
import lib.pyplotdefs as pd

# def cdf_inv(r: float, rho: float, sigma: float) -> float:
#     Mp = 2 * rho + sigma
#     Mm = 2 * rho - sigma
#     p = -3 * Mp / Mm
#     q = -8 * (r - 0.5) / Mm
#     if 4 * p**3 + 27 * q**2 > 0 and p < 0:
#         branch = True
#         t = (
#             -2 * (q / abs(q)) * np.sqrt(-p / 3)
#             * np.cosh(
#                 np.acosh((-3 * abs(q)) / (2 * p) * np.sqrt(-3 / p)) / 3
#             )
#         )
#     else:
#         branch = False
#         p = complex(p, 0.0)
#         q = complex(q, 0.0)
#         x0 = 2 * np.sqrt(-p / 3)
#         x1 = np.arccos((3 * q) / (2 * p) * np.sqrt(-3 / p)) / 3
#         for k in range(3):
#             tk = x0 * np.cos(x1 - 2 * np.pi * k / 3)
#             if tk.imag < 1e-6:
#                 break
#         t = tk.real
#         if tk.imag < 1e-6:
#             if tk.real >= -1 and tk.real <= 1:
#                 t = tk.real
#             elif abs(tk.real + 1.0) < 1e-6:
#                 t = -1.0
#             elif abs(tk.real - 1.0) < 1e-6:
#                 t = 1.0
#             else:
#                 raise Exception(
#                     f"cubic root is out of bounds\n"
#                     f"{r=} ; {rho=} ; {sigma=}\n"
#                     f"{tk=}"
#                 )
#         else:
#             raise Exception(
#                 f"cubic root is out of bounds\n"
#                 f"{r=} ; {rho=} ; {sigma=}\n"
#                 f"{tk=}"
#             )
#     return np.arccos(t), branch

def cdf_inv(r: float, rho: float, sigma: float) -> float:
    Mp = 2 * rho + sigma
    Mm = 2 * rho - sigma

    a = Mm / 8
    b = 0
    c = -3 * Mp / 8
    d = 0.5 - r

    D0 = complex(b**2 - 3 * a * c, 0.0)
    D1 = complex(2 * b**3 - 9 * a * b * c + 27 * a**2 * d, 0.0)
    Cp = pow((D1 + np.sqrt(D1**2 - 4 * D0**3)) / 2, 1 / 3)
    Cm = pow((D1 - np.sqrt(D1**2 - 4 * D0**3)) / 2, 1 / 3)
    if abs(Cp) > 1e-6:
        C = Cp
    elif abs(Cm) > 1e-6:
        C = Cm
    else:
        return -b / 3 / a
    xi = (-1 + np.sqrt(3) * 1j) / 2
    for k in [0, 1, 2]:
        x = -(b + xi**k * C + D0 / (xi**k * C)) / (3 * a)
        if abs(x.imag) < 1e-6:
            break
    if abs(x.imag) < 1e-6:
        if abs(x.real) <= 1.0:
            return np.arccos(x.real), k
        elif abs(x.real + 1.0) < 1e-6:
            return np.arccos(-1.0), k
        elif abs(x.real - 1.0) < 1e-6:
            return np.arccos(+1.0), k
        else:
            raise Exception(
                "cubic root is out of bounds\n"
                f"{r=} ; {rho=} ; {sigma=}\n"
                f"{x=}"
            )
    else:
        raise Exception(
            "cubic root is out of bounds\n"
            f"{r=} ; {rho=} ; {sigma=}\n"
            f"{x=}"
        )

def testpq(r: float, rho: float, sigma: float) -> float:
    Mp = 2 * rho + sigma
    Mm = 2 * rho - sigma
    p = -3 * Mp / Mm
    q = -8 * (r - 0.5) / Mm
    return 4 * p**3 + 27 * q**2, p

r_test = np.linspace(0.0, 1.0, 51)
rho_test = np.linspace(0.0, 1.0, 51)

# B = np.zeros((51, 51), dtype=np.float64)
# p = np.zeros((51, 51), dtype=np.float64)
# for i, r_t in enumerate(r_test):
#     for j, rho_t in enumerate(rho_test):
#         B[i, j], p[i, j] = testpq(r_t, rho_t, 1 - rho_t)
# pd.Plotter().imshow(B > 0).colorbar()
# pd.Plotter().imshow(p < 0).colorbar()

theta = np.zeros((51, 51), dtype=np.float64)
branch = np.zeros((51, 51), dtype=np.int8)
for i, r_t in enumerate(r_test):
    for j, rho_t in enumerate(rho_test):
        theta[i, j], branch[i, j] = cdf_inv(r_t, rho_t, 1 - rho_t)
pd.Plotter().imshow(theta).colorbar()
pd.Plotter().imshow(branch, cmap="gray").colorbar()

pd.show()

