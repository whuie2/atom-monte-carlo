import numpy as np
import lib.pyplotdefs as pd

saturation = 10.0
detuning = 1.0
linewidth = 5.0

def pop_excited(saturation: float, detuning: float, linewidth: float) -> float:
    return (
        saturation / 2.0
        / (1.0 + saturation + (2.0 * detuning / linewidth)**2)
    )

def rho_ee(t: float, saturation: float, detuning: float, linewidth: float) -> float:
    W = np.sqrt(linewidth / 2.0 * saturation)
    w = np.sqrt(W**2 - (linewidth / 4.0)**2 + detuning**2)
    return (
        pop_excited(saturation, detuning, linewidth)
        * (1.0 - np.exp(-0.75 * linewidth * t) * np.cos(w * t))
    )

def drho_ee(t: float, saturation: float, detuning: float, linewidth: float) -> float:
    W = np.sqrt(linewidth / 2.0 * saturation)
    w = np.sqrt(W**2 - (linewidth / 4.0)**2 + detuning**2)
    return (
        pop_excited(saturation, detuning, linewidth)
        * (
            0.75 * linewidth * np.exp(-0.75 * linewidth * t) * np.cos(w * t)
            + w * np.exp(-0.75 * linewidth * t) * np.sin(w * t)
        )
    )

def rho_ee_max(saturation: float, detuning: float, linewidth: float) -> (float, float):
    W = np.sqrt(linewidth / 2.0 * saturation)
    w = np.sqrt(W**2 - (linewidth / 4.0)**2 + detuning**2)
    t0 = (
        2.0 / w
        * np.arctan(
            4.0 * w / 3.0 / linewidth
            + np.sqrt(1.0 + (4.0 * w / 3.0 / linewidth)**2)
        )
    )
    rho0 = (
        pop_excited(saturation, detuning, linewidth)
        * (1.0 - np.exp(-0.75 * linewidth * t0) * np.cos(w * t0))
    )
    return (t0, rho0)

W = np.sqrt(linewidth / 2.0 * saturation)
w = np.sqrt(W**2 + (linewidth / 4.0)**2 + detuning**2)
t0, rho0 = rho_ee_max(saturation, detuning, linewidth)
K1 = np.exp(0.75 * linewidth * t0) - np.cos(w * t0)
K2 = 9.0 * linewidth**2 + 16.0 * w**2
tbar_anl = (
    12.0 * linewidth * K1
    - K2 * t0 * np.cos(w * t0)
     + 16.0 * w * np.sin(w * t0)
) / (K1 * K2)

t = np.linspace(0.0, t0, 10000)
tbar_num = np.trapz(
    (
        t * drho_ee(t, saturation, detuning, linewidth) / rho0
    ),
    dx=t[1] - t[0]
)

print(f"{{Y: {linewidth}, w: {w}, t1: {t0}}}")
print(tbar_anl)
print(tbar_num)

(pd.Plotter()
    .plot(t, rho_ee(t, saturation, detuning, linewidth) / rho0, color="k")
    .plot(t, drho_ee(t, saturation, detuning, linewidth) / rho0, color="0.5")
    .axvline(tbar_anl, color="b")
    .axvline(tbar_num, color="r")
    .ggrid()
    .show()
    .close()
)

