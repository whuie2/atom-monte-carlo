#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

use anyhow::Result;
use pachinko::newton::NewtonError;

fn main() -> Result<()> {
    return Err(NewtonError::RKAErrorBound)?;
}

