//! Provides a collection of miscellaneous macros, traits, and functions for
//! general use.

/// Call `print!` and automatically flush.
#[macro_export]
macro_rules! print_flush {
    ( $fmt:literal, $( $val:expr ),* ) => {
        print!($fmt, $( $val, )*);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
    }
}

/// Call `println!` and automatically flush.
#[macro_export]
macro_rules! println_flush {
    ( $fmt:literal, $( $val:expr ),* ) => {
        println!($fmt, $( $val, )*);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
    }
}

/// Create a directory, passed as a `PathBuf`, if it doesn't exist.
///
/// Creates all nonexisting parent directories as well.
#[macro_export]
macro_rules! mkdir {
    ( $dir_pathbuf:expr ) => {
        println!(":: mkdir -p {}", $dir_pathbuf.to_str().unwrap());
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
        std::fs::create_dir_all($dir_pathbuf.as_path())
            .expect(
                format!(
                    "Couldn't create directory {:?}",
                    $dir_pathbuf.to_str().unwrap()
                ).as_str()
            );
    }
}

/// Define a simple function, `load_config` to read and process independent
/// values from a table in a `.toml` file and return them as a `Config` struct.
///
/// Each value to be read is specified as a key (`&'static str`), a default
/// value and its type, the name and type of the `Config` struct field it's
/// stored under, and a closure to map the value read from the file (or the
/// default) to the one stored in the struct. Can only be used once per
/// namespace.
#[macro_export]
macro_rules! config_fn {
    (
        $subtab:expr => {
            $( $key:expr, $def:expr, $intype:ty
                => $field:ident : $outtype:ty = $foo:expr ),+ $(,)?
        }
    ) => {
        #[derive(Clone, Debug)]
        pub struct Config {
            $(
                pub $field: $outtype,
            )+
        }

        pub fn load_config(infile: std::path::PathBuf) -> Config {
            let infile: std::path::PathBuf
                = infile.unwrap_or(std::path::PathBuf::from($file));
            let table: toml::Value
                = std::fs::read_to_string(infile.clone())
                .expect(
                    format!("Couldn't read config file {:?}", infile)
                    .as_str()
                )
                .parse::<toml::Value>()
                .expect(
                    format!("Couldn't parse config file {:?}", infile)
                    .as_str()
                );
            let mut config = Config {
                $(
                    $field: ($foo)($def),
                )+
            };
            if let Some(value) = table.get($subtab) {
                $(
                    if let Some(X) = value.get($key) {
                        let x: $intype
                            = X.clone().try_into()
                            .expect(format!(
                                "Couldn't coerce type for key {:?}", $key)
                                .as_str()
                            );
                        config.$field = ($foo)(x);
                    }
                )+
            }
            return config;
        }
    }
}

/// Handy macro to create `num_complex::Complex64`s from more natural and
/// succinct syntax.
#[macro_export]
macro_rules! c {
    ( $re:literal + $im:literal i )
        => { num_complex::Complex64::new($re, $im) };
    ( $re:literal - $im:literal i )
        => { num_complex::Complex64::new($re, -$im) };
    ( $re:literal )
        => { num_complex::Complex64::new($re, 0.0) };
    ( $im:literal i )
        => { num_complex::Complex64::new(0.0, $im) };
    ( $re:expr, $im:expr )
        => { num_complex::Complex64::new($re, $im) };
}

/// Handles repeated calls with varied inputs to a closure outputting some
/// number of values, storing them in `ndarray::Array`s of appropriate shape.
///
/// Suppose we have some function $`f`$ that is defined over $`N`$ input
/// variables and returns $`M`$ output values. Given $`N`$ arrays of lengths
/// $`n_k`$ ($`k = 0, \dots, N - 1`$) sampling values over the $`N`$ variables,
/// this macro calls $`f`$ on each element of the Cartesian product of all $`N`$
/// arrays and collects the outputs of $`f`$ into $`M`$ arrays whose $`k`$-th
/// axis is associated with the $`k`$-th input array.
///
/// When calling this macro, the function `$caller` should take a single
/// `Vec<usize>` of indices, which should be used in the function body to refer
/// to input values in the sampling arrays, which in turn should be defined
/// beforehand, outside the macro. Output arrays must be returned as `IxDyn`.
///
/// # Example
/// ```
/// use ndarray::{ Array1, Array3, ArrayD, array };
/// use smorgasbord::{ loop_call };
///
/// let var1: Array1<f64> = array![0.5, 1.0, 2.0];
/// let var2: Array1<i32> = array![-1, 0, 1, 2];
/// let var3: Array1<bool> = array![true, false];
///
/// let caller
///     = |Q: Vec<usize>| -> (f64, f64) {
///         let val: f64 = var1[Q[0]].powi(var2[Q[1]]);
///         if var3[Q[2]] {
///             (val, 2.0 * val)
///         } else {
///             (-val, -2.0 * val)
///         }
///     };
/// let (ret1, ret2): (ArrayD<f64>, ArrayD<f64>)
///     = loop_call!(
///         caller => ( ret1: f64, ret2: f64 ),
///         vars: { var1, var2, var3 }
///     );
///
/// let ret1: Array3<f64> = ret1.into_dimensionality().unwrap();
/// let ret2: Array3<f64> = ret2.into_dimensionality().unwrap();
///
/// assert_eq!(
///     ret1,
///     array![
///         [
///             [ 2.00, -2.00 ],
///             [ 1.00, -1.00 ],
///             [ 0.50, -0.50 ],
///             [ 0.25, -0.25 ],
///         ],
///         [
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///         ],
///         [
///             [ 0.50, -0.50 ],
///             [ 1.00, -1.00 ],
///             [ 2.00, -2.00 ],
///             [ 4.00, -4.00 ],
///         ],
///     ]
/// );
/// assert_eq!(
///     ret2,
///     array![
///         [
///             [ 4.00, -4.00 ],
///             [ 2.00, -2.00 ],
///             [ 1.00, -1.00 ],
///             [ 0.50, -0.50 ],
///         ],
///         [
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///         ],
///         [
///             [ 1.00, -1.00 ],
///             [ 2.00, -2.00 ],
///             [ 4.00, -4.00 ],
///             [ 8.00, -8.00 ],
///         ],
///     ]
/// );
/// ```
#[macro_export]
macro_rules! loop_call {
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? } $(,)?
    ) => {
        loop_call!(
            $caller => ( $( $rvar: $rtype ),+ ),
            vars: { $( $var ),+ },
            printflag: true,
            lspace: 2
        )
    };
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? },
        printflag: $printflag:expr $(,)?
    ) => {
        loop_call!(
            $caller => ( $( $rvar: $rtype ),+ ),
            vars: { $( $var ),+ },
            printflag: $printflag,
            lspace: 2
        )
    };
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? },
        printflag: $printflag:expr,
        lspace: $lspace:expr $(,)?
    ) => {
        {
            let _caller_ = $caller;
            let _Nvals_: Vec<usize> = vec![ $( $var.len() ),+ ];
            let _nvars_: usize = _Nvals_.len();
            let _Z_: Vec<usize>
                = _Nvals_.iter()
                .map(|n| (*n as f64).log10().floor() as usize + 1)
                .collect();
            let _tot_: usize = _Nvals_.iter().product();
            let mut _NN_: Vec<usize>
                = (1.._nvars_).rev()
                .map(|k| _Nvals_[_nvars_ - k.._nvars_].iter().product())
                .collect();
            _NN_.push(1);

            let _mk_outstr_ = |Q: Vec<usize>, last: bool| -> String {
                let mut outstr_items: Vec<String>
                    = Vec::with_capacity(_nvars_ + 3);
                outstr_items.push(" ".repeat($lspace));
                for (k, idx) in Q.iter().enumerate() {
                    outstr_items.push(
                        format!("{0:2$}/{1:2$};  ",
                            idx + !last as usize, _Nvals_[k], _Z_[k]));
                }
                outstr_items.push(
                    format!(
                        "[{:6.2}%] \r",
                        100.0 * (
                            (0.._nvars_)
                                .map(|k| (Q[k] - last as usize) * _NN_[k])
                                .sum::<usize>() as f64
                            + last as usize as f64
                        ) / _tot_ as f64,
                    )
                );
                outstr_items.iter().flat_map(|s| s.chars()).collect()
            };

            let _input_idx_
                = itertools::Itertools::multi_cartesian_product(
                    _Nvals_.iter().map(|n| 0..*n)
                );
            let mut _outputs_: Vec<( $( $rtype ),+ ,)>
                = Vec::with_capacity(_tot_);
            let _t0_: std::time::Instant = std::time::Instant::now();
            for Q in _input_idx_ {
                if $printflag {
                    print!("{}", _mk_outstr_(Q.clone(), false));
                    std::io::Write::flush(&mut std::io::stdout()).unwrap();
                }
                _outputs_.push($caller(Q.clone()));
            }
            let _dt_: std::time::Duration = std::time::Instant::now() - _t0_;
            if $printflag {
                println!("{}", _mk_outstr_(_Nvals_.clone(), true));
                std::io::Write::flush(&mut std::io::stdout()).unwrap();
                println!(
                    "{}_tot_al time elapsed: {:.3} s",
                    " ".repeat($lspace),
                    _dt_.as_secs_f32(),
                );
                println!(
                    "{}average time per call: {:.3} s",
                    " ".repeat($lspace),
                    _dt_.as_secs_f32() / _tot_ as f32,
                );
            }

            let ( $( $rvar ),+ ,): ( $( Vec<$rtype> ),+ ,)
                = itertools::Itertools::multiunzip(_outputs_.into_iter());
            (
                $(
                    ndarray::Array::from_vec($rvar)
                        .into_shape(_Nvals_.as_slice())
                        .expect("couldn't reshape")
                ),+
            ,)
        }
    };
}

/// Trait to find the max (or argmax) of a collection of floating-point values
/// (since `f64` and `f32` do not implement `Ord`).
pub trait FExtremum<F> {
    fn fmax(&self) -> Option<F>;

    fn fmin(&self) -> Option<F>;

    fn fmax_idx(&self) -> Option<(usize, F)>;

    fn fmin_idx(&self) -> Option<(usize, F)>;
}

macro_rules! impl_fextremum {
    ( $f:ty ) => {
        fn fmax(&self) -> Option<$f> {
            return self.iter()
                .max_by(|l, r| {
                    l.partial_cmp(r)
                        .unwrap_or(std::cmp::Ordering::Greater)
                })
                .map(|v| *v);
        }

        fn fmin(&self) -> Option<$f> {
            return self.iter()
                .min_by(|l, r| {
                    l.partial_cmp(r)
                        .unwrap_or(std::cmp::Ordering::Less)
                })
                .map(|v| *v);
        }

        fn fmax_idx(&self) -> Option<(usize, $f)> {
            return self.iter().enumerate()
                .max_by(|(_kl, yl), (_kr, yr)| {
                    yl.partial_cmp(yr)
                        .unwrap_or(std::cmp::Ordering::Greater)
                })
                .map(|(k, y)| (k, *y));
        }

        fn fmin_idx(&self) -> Option<(usize, $f)> {
            return self.iter().enumerate()
                .min_by(|(_kl, yl), (_kr, yr)| {
                    yl.partial_cmp(yr)
                        .unwrap_or(std::cmp::Ordering::Less)
                })
                .map(|(k, y)| (k, *y));
        }
    }
}

macro_rules! impl_fextremum_simple {
    ( $hasiter:ty, $f:ty ) => {
        impl FExtremum<$f> for $hasiter {
            impl_fextremum!($f);
        }
    }
}

impl_fextremum_simple!(Vec<f64>, f64);
impl_fextremum_simple!(Vec<f32>, f32);
impl_fextremum_simple!([f64], f64);
impl_fextremum_simple!([f32], f32);

// pub fn find_first_max<'a, A: 'a, I>(y: I) -> (usize, A)
// where A: Copy + PartialOrd<A>,
//       I: Iterator<Item=&'a A>,
// {
//     let mut iter = y.peekable();
//     let mut y0: A = **iter.peek().unwrap();
//     let mut k0: usize = 0;
//     for (k, yk) in iter.enumerate() {
//         if yk <= &y0 {
//             break;
//         } else {
//             y0 = *yk;
//             k0 = k;
//         }
//     }
//     return (k0, y0);
// }
//
// pub fn find_first_min<'a, A: 'a, I>(y: I) -> (usize, A)
// where A: Copy + PartialOrd<A>,
//       I: Iterator<Item=&'a A>,
// {
//     let mut iter = y.peekable();
//     let mut y0: A = **iter.peek().unwrap();
//     let mut k0: usize = 0;
//     for (k, yk) in iter.enumerate() {
//         if yk >= &y0 {
//             break;
//         } else {
//             y0 = *yk;
//             k0 = k;
//         }
//     }
//     return (k0, y0);
// }

