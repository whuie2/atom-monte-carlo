#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

//! Library for various atomic physics-related Monte Carlo simulations.
//!
//! Provided implementors of library-owned traits assume SI units.

pub mod nd_utils;
pub mod phys;
pub mod utils;
// pub mod math;
pub mod newton;
pub mod trap;
pub mod atom;
pub mod recapture;
pub mod scatter;

