//! Provides the `Trap` trait, which describes a potential in which an atom may
//! move.

pub mod tweezer;

use std::f64::consts::TAU;
use rand::{
    self as rnd,
    Rng,
};
use crate::{
    newton::{
        ThreeVector,
        PhaseSpace,
    },
    phys::h,
};

/// Describes relevant properties that a trapping potential should have.
pub trait Trap: Copy + Clone {
    /// Trap center.
    const CENTER: ThreeVector;

    /// Get the location of the trap center.
    fn center(&self) -> ThreeVector { Self::CENTER }

    /// Get the trap depth. Positive depth should correspond to an attractive
    /// potential.
    fn depth(&self) -> f64;

    /// Potential energy function $`U(\vec{r})`$.
    fn potential(&self, r: ThreeVector) -> f64;

    /// Position-dependent light shift. Default implementation is
    /// `self.potential(r) / h`. Override this for non-SI units.
    fn light_shift(&self, r: ThreeVector) -> f64 { self.potential(r) / h }

    /// Gradient of the potential energy function, $`\nabla U(\vec{r})`$.
    fn gradient(&self, r: ThreeVector) -> ThreeVector;

    /// Forcing from the potential as $`-\nabla U(\vec{r})`$.
    fn force(&self, r: ThreeVector) -> ThreeVector { -self.gradient(r) }

    /// Should be returned in units of time^-1 (angular frequency).
    fn frequency(&self, mass: f64) -> f64;

    /// Should be returned in units of time.
    fn period(&self, mass: f64) -> f64 { TAU / self.frequency(mass) }

    /// Implements a criterion for whether an atom with a given [`PhaseSpace`]
    /// vector is considered "trapped."
    fn is_trapped(&self, mass: f64, q: PhaseSpace) -> bool;

    /// Sample an initial position in the trap from a thermal distribution.
    fn sample_position_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized;

    /// Sample an initial position in the trap from a thermal distribution.
    fn sample_position(&self, mass: f64, temperature: f64) -> ThreeVector {
        let mut rng = rnd::thread_rng();
        return self.sample_position_rng(mass, temperature, &mut rng);
    }

    /// Sample an initial velocity in the trap from a thermal distribution.
    fn sample_velocity_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized;

    /// Sample an initial velocity in the trap from a thermal distribution.
    fn sample_velocity(&self, mass: f64, temperature: f64) -> ThreeVector {
        let mut rng = rnd::thread_rng();
        return self.sample_velocity_rng(mass, temperature, &mut rng);
    }

    /// Sample an initial momentum in the trap from a thermal distribution.
    fn sample_momentum_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized;

    /// Sample an initial momentum in the trap from a thermal distribution.
    fn sample_momentum(&self, mass: f64, temperature: f64) -> ThreeVector {
        return mass * self.sample_velocity(mass, temperature);
    }

    /// Sample an initial phase-space vector from a thermal distribution.
    fn sample_phasespace_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> PhaseSpace
    where R: Rng + ?Sized;

    /// Sample an initial phase-space vector from a thermal distribution.
    fn sample_phasespace(&self, mass: f64, temperature: f64) -> PhaseSpace {
        let mut rng = rnd::thread_rng();
        return self.sample_phasespace_rng(mass, temperature, &mut rng);
    }
}

