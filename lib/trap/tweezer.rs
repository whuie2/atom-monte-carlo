//! Implements [`Trap`] for an optical tweezer (narrow-waisted Gaussian beam)
//! trapping potential.

use std::f64::consts::PI;
use rand::{
    Rng,
    distributions::Distribution,
};
use statrs::distribution::Normal;
use crate::{
    newton::{
        ThreeVector,
        PhaseSpace,
        Axis,
    },
    phys::{
        hbar,
        kB,
    },
    trap::Trap,
};

#[derive(Copy, Clone, Debug)]
pub struct Tweezer {
    /// Half the minimum diameter.
    pub waist: f64,
    pub rayleigh_range: f64,
    pub wavelength: f64,
    /// Maximum difference in energy relative to free space. Positive depths
    /// correspond to conservative potential.
    pub depth: f64,
    pub axis: Axis,
}

/// Describes an optical tweezer, parameterized by its 
impl Tweezer {
    /// Create a new tweezer.
    ///
    /// Positive depth corresponds to a conservative potential.
    pub fn new(waist: f64, wavelength: f64, depth: f64, axis: Axis) -> Self {
        let rayleigh_range: f64 = PI * waist.powi(2) / wavelength.abs();
        return Tweezer {
            waist: waist.abs(),
            rayleigh_range: rayleigh_range.abs(),
            wavelength: wavelength.abs(),
            depth,
            axis,
        };
    }

    /// Trap radius as a function of axial position.
    pub fn radius(&self, axial_pos: f64) -> f64 {
        return
            self.waist * (
                1.0
                + (axial_pos / self.rayleigh_range).powi(2)
            ).sqrt();
    }
}

impl Trap for Tweezer {
    const CENTER: ThreeVector = ThreeVector(0.0, 0.0, 0.0);

    fn depth(&self) -> f64 { self.depth }

    fn potential(&self, r: ThreeVector) -> f64 {
        let radius: f64 = self.radius(r.get_component(self.axis));
        let transverse: (f64, f64) = r.get_components_except(self.axis);
        return
            -self.depth
            * (self.waist / radius).powi(2)
            * (
                -2.0 * (transverse.0.powi(2) + transverse.1.powi(2))
                / radius.powi(2)
            ).exp();
    }

    fn gradient(&self, r: ThreeVector) -> ThreeVector {
        let axial: f64 = r.get_component(self.axis);
        let radius: f64 = self.radius(axial);
        let transverse: (f64, f64) = r.get_components_except(self.axis);
        let radial2: f64 = transverse.0.powi(2) + transverse.1.powi(2);
        let exp: f64 = (-2.0 * radial2 / radius.powi(2)).exp();
        let grad_axial: f64
            = 2.0 * self.depth
            * axial / self.rayleigh_range.powi(2)
            * (self.waist.powi(2) * radius - 2.0 * self.waist * radial2)
                / radius.powi(3)
            * exp;
        let grad_transverse0: f64
            = 4.0 * self.depth
            * transverse.0 / radius.powi(2)
            * exp;
        let grad_transverse1: f64
            = 4.0 * self.depth
            * transverse.1 / radius.powi(2)
            * exp;
        return match self.axis {
            Axis::X
                => ThreeVector(grad_axial, grad_transverse0, grad_transverse1),
            Axis::Y
                => ThreeVector(grad_transverse0, grad_axial, grad_transverse1),
            Axis::Z
                => ThreeVector(grad_transverse0, grad_transverse1, grad_axial),
        };
    }

    fn frequency(&self, mass: f64) -> f64 {
        return ((4.0 * self.depth) / (mass * self.waist.powi(2))).sqrt();
    }

    fn is_trapped(&self, mass: f64, q: PhaseSpace) -> bool {
        let kin: f64 = q.mom.norm().powi(2) / (2.0 * mass);
        let pot: f64 = self.potential(q.pos);
        return kin + pot < 0.0;
    }

    fn sample_position_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized
    {
        let omega_radial: f64 // radial trap frequency
            = ((4.0 * self.depth) / (mass * self.waist.powi(2))).sqrt();
        let X_radial: f64 // relevant Boltzmann factor argument
            = (hbar * omega_radial) / (kB * temperature);
        let sigma_radial: f64 // radial position spread
            = (
                hbar / mass / omega_radial
                * (0.5 + 1.0 / (X_radial.exp() - 1.0))
            ).sqrt();

        let omega_axial: f64 // axial trap frequency
            = (
                (2.0 * self.depth)
                / (mass * self.rayleigh_range.powi(2))
            ).sqrt();
        let X_axial: f64 // relevant Boltzmann factor argument
            = -(hbar * omega_axial) / (2.0 * kB * temperature);
        let sigma_axial: f64 // axial position spread
            = (
                hbar / mass / omega_axial
                * (0.5 + 1.0 / (X_axial.exp() - 1.0))
            ).sqrt();

        let dist_radial = Normal::new(0.0, sigma_radial).unwrap();
        let dist_axial = Normal::new(0.0, sigma_axial).unwrap();

        let pos_radial: (f64, f64)
            = (dist_radial.sample(rng), dist_radial.sample(rng));
        let pos_axial: f64 = dist_axial.sample(rng);

        return match self.axis {
            Axis::X => ThreeVector(pos_axial, pos_radial.0, pos_radial.1),
            Axis::Y => ThreeVector(pos_radial.0, pos_axial, pos_radial.1),
            Axis::Z => ThreeVector(pos_radial.0, pos_radial.1, pos_axial),
        };
    }

    fn sample_velocity_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized
    {
        let v_rms: f64 = (kB * temperature / mass).sqrt();
        let dist = Normal::new(0.0, v_rms).unwrap();
        return ThreeVector(
            dist.sample(rng),
            dist.sample(rng),
            dist.sample(rng)
        );
    }

    fn sample_momentum_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> ThreeVector
    where R: Rng + ?Sized
    {
        return mass * self.sample_velocity_rng(mass, temperature, rng);
    }

    fn sample_phasespace_rng<R>(&self, mass: f64, temperature: f64, rng: &mut R)
        -> PhaseSpace
    where R: Rng + ?Sized
    {
        return PhaseSpace {
            pos: self.sample_position_rng(mass, temperature, rng),
            mom: self.sample_momentum_rng(mass, temperature, rng),
        };
    }
}

